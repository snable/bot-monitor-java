/*
 * Copyright (c) 2017 Baidu, Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.baidu.apm.model;

import com.github.wnameless.json.flattener.JsonFlattener;

import java.util.HashMap;
import java.util.Map;

public class Response {

    // http response info
    private Map<String, Object> response;

    public Response(String responseContent) {
        this.response = JsonFlattener.flattenAsMap(responseContent);
    }

    /**
     * get output speech type
     *
     * @return output speech type
     */
    public String getOutputSpeechType() {
        Object speechType = response.get("response.outputSpeech.type");
        if (speechType != null) {
            return speechType.toString();
        }
        return "";
    }

    /**
     * get output speech text
     *
     * @return output speech text
     */
    public String getOutputSpeechText() {
        Object speechText = response.get("response.outputSpeech.text");
        if (speechText != null) {
            return speechText.toString();
        }
        return "";
    }

    /**
     * get output speech ssml
     *
     * @return output speech ssml
     */
    public String getOutputSpeechSSML() {
        Object speechSSML = response.get("response.outputSpeech.ssml");;
        if (speechSSML != null) {
            return speechSSML.toString();
        }
        return "";
    }

    /**
     * get repromt type
     *
     * @return repromt type
     */
    public String getRepromptType() {
        Object speechType = response.get("response.reprompt.outputSpeech.type");
        if (speechType != null) {
            return speechType.toString();
        }
        return "";
    }

    /**
     * get reprompt text
     *
     * @return reprompt text
     */
    public String getRepromptText() {
        Object speechText = response.get("response.reprompt.outputSpeech.text");
        if (speechText != null) {
            return speechText.toString();
        }
        return "";
    }

    /**
     * get reprompt ssml
     *
     * @return reprompt ssml
     */
    public String getRepromptSSML() {
        Object speechSSML = response.get("response.reprompt.outputSpeech.ssml");
        if (speechSSML != null) {
            return speechSSML.toString();
        }
        return "";
    }

    /**
     * get should end session
     *
     * @return should end session
     */
    public Boolean getShouldEndSession() {
        return Boolean.getBoolean(this.response.get("response.shouldEndSession").toString());
    }

    /**
     * get slot name
     *
     * @return slot name
     */
    public String getSlotName() {
        Object slotName = this.response.get("response.directives[0].slotToElicit");
        if (slotName != null) {
            return slotName.toString();
        }
        return "";
    }

    /**
     * get output speech
     *
     * @return output speech
     */
    public Object getOutputSpeech() {
        Map<String, String> outputSpeech = new HashMap<>();
        outputSpeech.put("type", this.getOutputSpeechType());
        outputSpeech.put("text", this.getOutputSpeechText());
        outputSpeech.put("ssml", this.getOutputSpeechSSML());
        return outputSpeech;
    }

    /**
     * get reprompt
     *
     * @return reprompt
     */
    public Object getReprompt() {
        Map<String, String> reprompt = new HashMap<>();
        reprompt.put("type", this.getRepromptType());
        reprompt.put("text", this.getRepromptText());
        reprompt.put("ssml", this.getRepromptSSML());
        return reprompt;
    }
}
