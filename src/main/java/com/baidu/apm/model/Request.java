/*
 * Copyright (c) 2017 Baidu, Inc. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.baidu.apm.model;

import com.github.wnameless.json.flattener.JsonFlattener;

import java.util.HashMap;
import java.util.Map;

public class Request {

    // convert request string to map
    private Map<String, Object> request;

    public Request(String requestContent) {
        this.request = JsonFlattener.flattenAsMap(requestContent);
    }

    /**
     * get bot id
     *
     * @return botId
     */
    public String getBotId() {
        Object botId = request.get("context.System.application.applicationId");
        if (botId != null) {
            return botId.toString();
        }
        return "";
    }

    /**
     * get request id
     *
     * @return request id
     */
    public String getRequestId() {
        Object requestid = request.get("request.requestId");
        if (requestid != null) {
            return requestid.toString();
        }
        return "";
    }

    /**
     * get query
     *
     * @return query
     */
    public String getQuery() {
        Object query = request.get("request.query.original");
        if (query != null) {
            return query.toString();
        }
        return "";
    }


    /**
     * get failed reason
     *
     * @return reason
     */
    public String getReason() {
        Object reason = request.get("request.reason");
        if (reason != null) {
            return reason.toString();
        }
        return "";
    }


    /**
     * get device id
     *
     * @return device id
     */
    public String getDeviceId() {
        Object deviceId = request.get("context.System.device.deviceId");
        if (deviceId != null) {
            return deviceId.toString();
        }
        return "";
    }

    /**
     * get request type
     *
     * @return request type
     */
    public String getRequestType() {
        Object requestType = request.get("request.type");
        if (requestType != null) {
            return requestType.toString();
        }
        return "";
    }

    /**
     * get user id
     *
     * @return user id
     */
    public String getUserId() {
        Object userId = request.get("context.System.user.userId");
        if (userId != null) {
            return userId.toString();
        }
        return "";
    }

    /**
     * get intent name
     * @param index intent name index
     *
     * @return intent name
     */
    public String getIntentNameByIndex(Integer index) {
        String intentKey = "request.intents[" + index.toString() + "].name";
        Object intentName = request.get(intentKey);
        if (intentName != null) {
            return intentName.toString();
        }
        return "";
    }

    /**
     * get sessionId
     *
     * @return sessionId
     */
    public String getSessionId() {
        Object sessionId = request.get("session.sessionId");
        if (sessionId != null) {
            return sessionId.toString();
        }
        return "";
    }

    /**
     * get user location info
     *
     * @return a map contains user location
     */
    public Map<Object, Object> getLocation() {
        Object bd09llLongitude = request.get("context.System.user.userInfo.location.geo.bd09ll.longitude");
        Object bd09llLatitude = request.get("context.System.user.userInfo.location.geo.bd09ll.latitude");
        if (bd09llLongitude == null) {
            return new HashMap<>();
        }
        Object wgs84Longitude = request.get("context.System.user.userInfo.location.geo.wgs84.longitude");
        Object wgs84lLatitude = request.get("context.System.user.userInfo.location.geo.wgs84.latitude");
        Object bd09mcLongitude = request.get("context.System.user.userInfo.location.geo.bd09mc.longitude");
        Object bd09mcLatitude = request.get("context.System.user.userInfo.location.geo.bd09mc.latitude");

        Map<Object, Object> retMap = new HashMap<>();

        Map<String, Double> bd09llData = new HashMap<>();
        bd09llData.put("longitude", Double.valueOf(bd09llLongitude.toString()));
        bd09llData.put("latitude", Double.valueOf(bd09llLatitude.toString()));
        retMap.put("bd09ll", bd09llData);

        Map<String, Double> wgs84Data = new HashMap<>();
        wgs84Data.put("longitude", Double.valueOf(wgs84Longitude.toString()));
        wgs84Data.put("latitude", Double.valueOf(wgs84lLatitude.toString()));
        retMap.put("wgs84", wgs84Data);

        Map<String, Double> bd09mcData = new HashMap<>();
        bd09mcData.put("longitude", Double.valueOf(bd09mcLongitude.toString()));
        bd09mcData.put("latitude", Double.valueOf(bd09mcLatitude.toString()));
        retMap.put("bd09mc", wgs84Data);

        return retMap;
    }
}